/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Main Program
 * Draws triangles to a VTK image then outputs the image to disc.
 */
#include <iostream>
#include <vtkDataSet.h>
#include <vtkImageData.h>
#include <vtkPNGWriter.h>
#include <vtkPointData.h>
#include <getopt.h>
#include <glog/logging.h>

#include "Utility.h"
#include "Triangle.h"
#include "Screen.h"
#include "Camera.h"
#include "Matrix.h"

using std::cerr;
using std::endl;

int main(int argc, char **argv) {
  google::InitGoogleLogging(argv[0]);

  // Default values
  bool use_old = false;
  bool debug = false;
  bool record_video = false;
  int width = 1000;
  int height = 1000;
  int frame_num = -1;
  int begin = -1;
  int size = -1;
  int opt;
  std::string out_file = "output";
  std::string in_file = "res/proj1e_geometry.vtk";

  // Parse incoming options
  while ((opt = getopt (argc, argv, "f:h:w:o:i:s:b:zdv")) != -1) {
    switch (opt)
    {
      case 'f':
	frame_num = atoi(optarg);
	break;
      case 'v':
	record_video;
	break;
      case 'd':
	debug = true;
	break;
      case 'b':
	begin = atoi(optarg);
	break;
      case 's':
	size = atoi(optarg);
	break;
      case 'z':
	use_old = true;
	break;
      case 'h':
	height = atoi(optarg);
	break;
      case 'o':
	out_file = std::string(optarg);
	break;
      case 'i':
	in_file = std::string(optarg);
	break;
      case 'w':
	width = atoi(optarg);
	break;
      default:
	break;
    }
  }

  // Setup out canvas
  // Iterate through all pixels, painting them black
  vtkImageData *image = NewImage(width, height);
  ClearImage(width, height, image);
  
  // Setup the 'screen'
  unsigned char *buffer = (unsigned char *) image->GetScalarPointer(0,0,0);
  Screen screen(width, height, buffer, debug);

  // Get triangles from Hank's magic triangle generating function
  std::vector<Triangle> triangles;
  if (use_old) {
    triangles = GetTriangles100();
  } else {
    triangles = GetTriangles(in_file);
  }

  // triangles[708].rasterize(screen);
  LightingParameters lp;
  std::vector<Triangle> *triangles_window;
  if (begin != -1) {
    if (size != -1)
      triangles_window = new std::vector<Triangle>(triangles.begin() + begin, triangles.begin() + begin + size);
    else
      triangles_window = new std::vector<Triangle>(triangles.begin() + begin, triangles.begin() + begin + 1);
  } else {
    // Use _all_ triangles if not trying to debug...
    triangles_window = &triangles;
  }

  if (record_video) 
  {
    cout << "TODO" << endl;;
  }
  else  if (frame_num != -1)
  {
    // Get our camera 
    Matrix transform;
    Camera cam = Camera::GetCamera(frame_num,1000);
    transform = Matrix::ComposeMatrices(cam.CameraTransform(), cam.ViewTransform());
    transform = Matrix::ComposeMatrices(transform, cam.DeviceTransform(width, height));

    // Transform and rasterize triangles
    Vector cam_position(cam.position);
    std::vector<Triangle> triangles_draw = transformTriangles(*triangles_window, transform, cam_position, lp);
    Vector vd = cam.getViewDirection();
    rasterizeTriangles(triangles_draw, screen);   
    
    // Write the image 
    WriteImage(image, out_file.c_str());
  } else {
    for (int i = 0; i < 1000; i = i + 250) {
      // Get our camera 
      Matrix transform;
      Camera cam = Camera::GetCamera(i,1000);
      transform = Matrix::ComposeMatrices(cam.CameraTransform(), cam.ViewTransform());
      transform = Matrix::ComposeMatrices(transform, cam.DeviceTransform(width, height));

      // Transform and rasterize triangles
      Vector cam_position(cam.position);
      std::vector<Triangle> triangles_draw = transformTriangles(*triangles_window, transform, cam_position, lp);
      Vector vd = cam.getViewDirection();
      screen.clear();
      rasterizeTriangles(triangles_draw, screen);   
      
      // Write the image 
      std::string out_file_num = out_file + "_" +  std::to_string(i);
      WriteImage(image, out_file_num.c_str());
    }
  }


  // TODO: This code will likely have to change to handle 'video' mode. and Default mode.
  if (debug) {
    // Just for debugging purposes
    screen.logScreenTriangleIds();
  }
}
