#include <cmath>

#include "Camera.h"
#include "Matrix.h"
#include "Vector.h"
#include "Utility.h"

Vector Camera::getViewDirection() {
  Vector origin = Vector(position);
  Vector focus_v = Vector(focus);
  Vector ret = focus_v - origin;
  //Vector ret = origin - focus_v;
  return ret.normalized();
}

Matrix Camera::ViewTransform() {
  Matrix ret;
  ret.reset();
  // 1st row
  ret.set(0,0,cot(angle/2));
  // 2nd row
  ret.set(1,1,cot(angle/2));
  // 3rd row
  ret.set(2,2,(far+near)/(far-near)); ret.set(3,2,-1);
  // 4th row
  ret.set(2,3,(2*far*near)/(far-near)); 
  return ret;
}

// Where t = (0,0,0) - origin;
//
// [v1.x, v2.x, v3.x, 0]
// [v1.y, v2.y, v3.y, 0]
// [v1.z, v2.z, v3.z, 0]
// [Dot(v1,t), Dot(v2,t), Dot(v3,t), 1]
Matrix Camera::CameraTransform() {
  // Calculate our camera frame
  Vector zero = Vector(0.0,0.0,0.0);
  Vector origin = Vector(position);
  Vector focus_v = Vector(focus);
  Vector up_v(up);
  Vector v1;
  Vector v2;
  Vector v3;
  v3 = origin - focus_v;
  v3.normalize();
  v1 = Vector::Cross(up_v,v3);
  v1.normalize();
  v2 = Vector::Cross(v3,v1);
  v2.normalize();
  Vector t = zero - origin;

  // Calculate our Camera Transform matrix now
  Matrix ret;
  ret.reset();
  // 1st row
  ret.set(0,0,v1.x); ret.set(1,0,v2.x); ret.set(2,0,v3.x);
  // 2nd row
  ret.set(0,1,v1.y); ret.set(1,1,v2.y); ret.set(2,1,v3.y);
  // 3rd row
  ret.set(0,2,v1.z); ret.set(1,2,v2.z); ret.set(2,2,v3.z);
  // 4th row
  ret.set(0,3,Vector::Dot(v1,t)); 
  ret.set(1,3,Vector::Dot(v2,t)); 
  ret.set(2,3,Vector::Dot(v3,t)); 
  ret.set(3,3,1);
  return ret;
}

// The device tranform matrix looks like the following.
// W = min(width,height)/2
// [W, 0, 0, 0]
// [0, W, 0, 0]
// [0, 0, 0, 0]
// [W, W, 0, 0]
Matrix Camera::DeviceTransform(unsigned int width, unsigned int height) {
  Matrix ret;
  ret.reset();
  unsigned int crop = std::min(width, height) / 2;
  ret.set(0,0, crop);
  ret.set(1,1, crop);
  ret.set(2,2, 1);
  ret.set(3,3, 1);
  ret.set(0,3, crop);
  ret.set(1,3, crop);
  return ret;
}

Camera Camera::GetCamera(int frame, int nframes) {
    double t = SineParameterize(frame, nframes, nframes/10);
    Camera c;
    c.near = 5;
    c.far = 200;
    c.angle = M_PI/6;
    c.position[0] = 40*sin(2*M_PI*t);
    c.position[1] = 40*cos(2*M_PI*t);
    c.position[2] = 40;
    c.focus[0] = 0;
    c.focus[1] = 0;
    c.focus[2] = 0;
    c.up[0] = 0;
    c.up[1] = 1;
    c.up[2] = 0;
    return c;
}


double Camera::SineParameterize(int curFrame, int nFrames, int ramp) {
    int nNonRamp = nFrames-2*ramp;
    double height = 1./(nNonRamp + 4*ramp/M_PI);
    if (curFrame < ramp)
    {
        double factor = 2*height*ramp/M_PI;
        double eval = cos(M_PI/2*((double)curFrame)/ramp);
        return (1.-eval)*factor;
    }
    else if (curFrame > nFrames-ramp)
    {
        int amount_left = nFrames-curFrame;
        double factor = 2*height*ramp/M_PI;
        double eval =cos(M_PI/2*((double)amount_left/ramp));
        return 1. - (1-eval)*factor;
    }
    double amount_in_quad = ((double)curFrame-ramp);
    double quad_part = amount_in_quad*height;
    double curve_part = height*(2*ramp)/M_PI;
    return quad_part+curve_part;
}

