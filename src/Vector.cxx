/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Vector Helper 
 */
#include <cmath>
#include <glog/logging.h>

#include "Vector.h"

Vector::Vector(double x, double y, double z) :
  x(x),
  y(y),
  z(z)
  {}

Vector::Vector() : 
  Vector(0.0,0.0,0.0) {}


Vector::Vector(double xyz[]) :
  Vector(xyz[0], xyz[1], xyz[2]) {}

Vector::Vector(const Vector &other) :
  Vector(other.x, other.y, other.z) {}

Vector Vector::operator-(const Vector &rhs) {
  return Vector(this->x - rhs.x, this->y - rhs.y, this->z - rhs.z);
}

Vector Vector::operator*(const double &rhs) {
  return Vector(this->x * rhs, this->y * rhs, this->z * rhs);
}

// Static
double Vector::Dot(const Vector &v1, const Vector &v2) {
  return (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
}

Vector Vector::Cross(const Vector &a, const Vector &b) {
  Vector ret;
  ret.x = a.y*b.z - a.z*b.y;
  ret.y = b.x*a.z - a.x*b.z;
  ret.z = a.x*b.y - a.y*b.x;
  return ret;
}

Vector Vector::normalized() {
  // calculate magnitude of vector
  double magnitude_sqr = Vector::Dot(*this,*this);
  if (magnitude_sqr == 0.0) {
    LOG(WARNING) << "Zero Magnitude (SQUARED)! Normalized vector getting (0,0,0)";
    return Vector(0.0,0.0,0.0);
  }
  double magnitude = std::sqrt(magnitude_sqr);
  if (magnitude == 0.0) {
    LOG(WARNING) << "Zero Magnitude! Normalized vector getting (0,0,0)";
    return Vector(0.0,0.0,0.0);
  }
  else if (magnitude < 0.0) {
    LOG(WARNING) << "Negative Magnitude -- flipping magnitude!";
    magnitude = -magnitude;
  }
  // Divide this vector by the magnitude of it
  return (*this) * (1.0/magnitude);
}

void Vector::normalize() {
  Vector tmp = this->normalized();
  this->x = tmp.x;
  this->y = tmp.y;
  this->z = tmp.z;
}
