/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Screen Management
 */
#include <glog/logging.h>

#include "Screen.h"
#include "Utility.h"

Screen::Screen(unsigned int _width, unsigned int _height, unsigned char *_buffer, bool debug) : 
  buffer (_buffer), 
  width(_width),
  height (_height),
  debug(debug)
{
  z_buffer = new double[width*height];
  std::fill(z_buffer, z_buffer + width*height, -1.0);
  if (debug) {
    triangle_ids = new unsigned int[width*height];
  }
}

Screen::Screen(unsigned int _width, unsigned int _height, unsigned char *_buffer) :
  Screen(_width, _height, _buffer, false)
  {}

Screen::~Screen() {
  delete[] z_buffer;
  if (debug) {
    delete[] triangle_ids;
  }
}

int Screen::coord_to_index(unsigned int x, unsigned int y) {
  return BYTES_PER_PIXEL * ((y * width) + x);
}

int Screen::zbuffer_coord_to_index(unsigned int x, unsigned int y) {
  return (y * width) + x;
}

/**
 * Get the zbuffer value of a pixel at a given location in this screen
 */
double Screen::getZ(unsigned int x, unsigned int y) {
  if (x >= width || y >= height) {
    // Too verbose: LOG(INFO) << "Failed attempt to get pixel from offscreen (" << x << ", " << y << ")";
    return -1.0;
  }
  unsigned int index = zbuffer_coord_to_index(x,y);
  return z_buffer[index];
}

/**
 * Set the zbuffer value of a pixel at a given location in this screen
 */
void Screen::setZ(unsigned int x, unsigned int y, double z) {
  if (x >= width || y >= height) {
    // Too verbose: LOG(INFO) << "Failed attempt to get pixel from offscreen (" << x << ", " << y << ")";
    return;
  }
  unsigned int index = zbuffer_coord_to_index(x,y);
  z_buffer[index] = z;
}

void Screen::setTriangleId(unsigned int x, unsigned int y, unsigned int id) {
  if (!debug) {
    return;
  }
  if (x >= width || y >= height) {
    // Too verbose: LOG(INFO) << "Failed attempt to get pixel from offscreen (" << x << ", " << y << ")";
    return;
  }
  unsigned int index = zbuffer_coord_to_index(x,y);
  this->triangle_ids[index] = id;
}

void Screen::logScreenTriangleIds() {
  if (!debug) {
    return;
  }
  unsigned int index;
  for (unsigned int x = 0; x < width; x++) {
    for (unsigned int y = 0; y < height; y++) {
      index = zbuffer_coord_to_index(x,y);
      LOG(INFO) << "Printed Triangle " << std::setw(7) << this->triangle_ids[index]
		<< " to pixel (" << std::setw(4) << x << ", " << std::setw(4) << y << ")";
    }
  }
}


void Screen::getPixel(unsigned int x, unsigned int y, unsigned char (&ret_color)[BYTES_PER_PIXEL]) {
  if (x >= width || y >= height) {
    // Too verbose: LOG(INFO) << "Failed attempt to get pixel from offscreen (" << x << ", " << y << ")";
    return;
  }
  unsigned int index = coord_to_index(x,y);
  unsigned char *pixel = &buffer[index];
  ret_color[RED_CH] = pixel[RED_CH];
  ret_color[GRN_CH] = pixel[GRN_CH];
  ret_color[BLU_CH] = pixel[BLU_CH];
}

void Screen::getPixel(unsigned int x, unsigned int y, double (&ret_color)[3]) {
  unsigned char color_bytes[3];
  getPixel(x, y, color_bytes);
  ret_color[RED_CH] = (color_bytes[RED_CH]/255.0);
  ret_color[GRN_CH] = (color_bytes[GRN_CH]/255.0);
  ret_color[BLU_CH] = (color_bytes[BLU_CH]/255.0);
}

void Screen::setPixel(unsigned int x, unsigned int y, const unsigned char color[BYTES_PER_PIXEL]) {
  if (x >= width || y >= height) {
    // Too verbose: LOG(WARNING) << "Failed attempted to set pixel offscreen (" << x << ", " << y << ")";
    return;
  }
  unsigned int index = coord_to_index(x,y);
  unsigned char *pixel = &buffer[index];
  pixel[RED_CH] = color[RED_CH];
  pixel[GRN_CH] = color[GRN_CH];
  pixel[BLU_CH] = color[BLU_CH];
}

void Screen::setPixel(unsigned int x, unsigned int y, const double color[BYTES_PER_PIXEL]) {
  unsigned char color_bytes[3];
  color_bytes[RED_CH] = ceil441(color[RED_CH]*255.0);
  color_bytes[GRN_CH] = ceil441(color[GRN_CH]*255.0);
  color_bytes[BLU_CH] = ceil441(color[BLU_CH]*255.0);
  setPixel(x, y, color_bytes);
}

void Screen::clear() {
  memset(buffer, 0, width * height * BYTES_PER_PIXEL);
  std::fill(z_buffer, z_buffer + width*height, -1.0);
}
