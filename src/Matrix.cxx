/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Matrix Helper
 */
#include <glog/logging.h>

#include "Matrix.h"

// row major styled call
void Matrix::set(unsigned int x, unsigned int y, double val) {
  if (x > 3 || y > 3 || x < 0 || y < 0) {
    LOG(WARNING) << "Matrix construction attempted to set invalid field";
    return;
  }
  A[y][x] = val;
}

// row major styled call
double Matrix::get(unsigned int x, unsigned int y) {
  if (x > 3 || y > 3 || x < 0 || y < 0) {
    LOG(WARNING) << "Matrix construction attempted to set invalid field";
    return -1.0; // TODO: What should we return if invalid?
  }
  return A[y][x];
}

void Matrix::reset() {
  for (unsigned int x = 0; x < 4; x++){
    for (unsigned int y = 0; y < 4; y++){
      A[x][y] = 0.0;
    }
  }
}

void Matrix::Print(std::ostream &o) {
    for (int i = 0 ; i < 4 ; i++) {
        char str[256];
        sprintf(str, "(%.7f %.7f %.7f %.7f)\n", A[i][0], A[i][1], A[i][2], A[i][3]);
        o << str;
    }
}

Matrix Matrix::ComposeMatrices(const Matrix &M1, const Matrix &M2) {
    Matrix rv;
    for (int i = 0 ; i < 4 ; i++) {
        for (int j = 0 ; j < 4 ; j++) {
	    // Sum ( First row of A times First Column of B )
	    // ret[0][0] = (B[0][0] * C[0][0]) + (B[0][1] * C[1][0]) + (B[0][2] * C[2][0]) + (B[0][3] * C[3][0]);
            rv.A[i][j] = 0;
            for (int k = 0 ; k < 4 ; k++)
                rv.A[i][j] += M1.A[i][k]*M2.A[k][j];
        }
    }
    return rv;
}

void Matrix::TransformPoint(const double *ptIn, double *ptOut) {
    ptOut[0] = ptIn[0]*A[0][0]
             + ptIn[1]*A[1][0]
             + ptIn[2]*A[2][0]
             + ptIn[3]*A[3][0];
    ptOut[1] = ptIn[0]*A[0][1]
             + ptIn[1]*A[1][1]
             + ptIn[2]*A[2][1]
             + ptIn[3]*A[3][1];
    ptOut[2] = ptIn[0]*A[0][2]
             + ptIn[1]*A[1][2]
             + ptIn[2]*A[2][2]
             + ptIn[3]*A[3][2];
    ptOut[3] = ptIn[0]*A[0][3]
             + ptIn[1]*A[1][3]
             + ptIn[2]*A[2][3]
             + ptIn[3]*A[3][3];
}
