/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Test Program
 */
#include <glog/logging.h>
#include <gtest/gtest.h>
#include <vtkImageData.h>
#include <vtkPNGWriter.h>
#include "Utility.h"
#include "Point.h"
#include "Screen.h"
#include "Triangle.h"
#include "Camera.h"

// Forward declard the 'screen' global
Screen *screen;
vtkImageData *image;

#define TEST_WIDTH 36
#define TEST_HEIGHT 36
const double WHITE[BYTES_PER_PIXEL][3] = {{1.0, 1.0, 1.0}, {1.0, 1.0, 1.0}, {1.0, 1.0, 1.0}};
const double BLACK[BYTES_PER_PIXEL][3] = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
LightingParameters ambient_lp = LightingParameters(0,0,0,1.0,0,0,0);
// const LightingParameters phong_lp = LightingParameters(0,0,0,1.0,0,0,0,0);

void EXPECT_PIXEL_IS_COLOR(unsigned int x, unsigned int y, const unsigned char expected_color[3]) { 
  unsigned char observed_color[BYTES_PER_PIXEL];
  screen->getPixel(x,y,observed_color);
  EXPECT_EQ(expected_color[RED_CH], observed_color[RED_CH]) << "Red Channel Broken at (" << x << ", " << y << ")";
  EXPECT_EQ(expected_color[GRN_CH], observed_color[GRN_CH]) << "Grn Channel Broken at (" << x << ", " << y << ")";
  EXPECT_EQ(expected_color[BLU_CH], observed_color[BLU_CH]) << "Blu Channel Broken at (" << x << ", " << y << ")";
}

void EXPECT_PIXEL_IS_COLOR(unsigned int x, unsigned int y, const double expected_color[3]) { 
  double observed_color[BYTES_PER_PIXEL];
  screen->getPixel(x,y,observed_color);
  EXPECT_EQ(expected_color[RED_CH], observed_color[RED_CH]) << "Red Channel Broken at (" << x << ", " << y << ")";
  EXPECT_EQ(expected_color[GRN_CH], observed_color[GRN_CH]) << "Grn Channel Broken at (" << x << ", " << y << ")";
  EXPECT_EQ(expected_color[BLU_CH], observed_color[BLU_CH]) << "Blu Channel Broken at (" << x << ", " << y << ")";
}

void EXPECT_PIXEL_IS_WHITE(unsigned int x, unsigned int y) { 
  EXPECT_PIXEL_IS_COLOR(x,y,WHITE[0]);
}

void EXPECT_PIXEL_IS_BLACK(unsigned int x, unsigned int y) { 
  EXPECT_PIXEL_IS_COLOR(x,y,BLACK[0]);
}

/*
// Setting then Getting Pixel testing
TEST(SetPixelGetPixel, FirstPixel) {
  screen->clear();

  screen->setPixel(0,0,WHITE[0]);

  EXPECT_PIXEL_IS_WHITE(0,0);
  WriteImage(image, "test_DotLowerLeft");
}

// Setting then Getting Pixel testing
TEST(SetPixelGetPixel, VerticalLine) {
  screen->clear();

  screen->setPixel(5,3,WHITE[0]);
  screen->setPixel(5,4,WHITE[0]);
  screen->setPixel(5,5,WHITE[0]);
  screen->setPixel(5,6,WHITE[0]);
  screen->setPixel(5,7,WHITE[0]);

  EXPECT_PIXEL_IS_WHITE(5,3);
  EXPECT_PIXEL_IS_WHITE(5,4);
  EXPECT_PIXEL_IS_WHITE(5,5);
  EXPECT_PIXEL_IS_WHITE(5,6);
  EXPECT_PIXEL_IS_WHITE(5,7);
  WriteImage(image, "test_VerticalLine");
}

// Setting then Getting Pixel testing
TEST(Screen, ZBufferInit) {
  screen->clear();

  ASSERT_FLOAT_EQ(-1.0, screen->getZ(0, 0));
  ASSERT_FLOAT_EQ(-1.0, screen->getZ(10, 10));
  ASSERT_FLOAT_EQ(-1.0, screen->getZ(30, 32));
  ASSERT_FLOAT_EQ(-1.0, screen->getZ(TEST_WIDTH-1, TEST_HEIGHT-1));
}

// Setting then Getting Pixel testing
TEST(Screen, ZBuffer) {
  screen->clear();

  screen->setZ(TEST_WIDTH-1, TEST_HEIGHT-1, 0.0);
  screen->setZ(10, 10, 0.0);
  screen->setZ(0, 0, 0.0);

  ASSERT_FLOAT_EQ(0.0, screen->getZ(TEST_WIDTH-1, TEST_HEIGHT-1));
  ASSERT_FLOAT_EQ(0.0, screen->getZ(10, 10));
  ASSERT_FLOAT_EQ(0.0, screen->getZ(0, 0));
}


// Setting then Getting Pixel testing
TEST(SetPixelGetPixel, Triangle) {
  screen->clear();

  screen->setPixel(1,1,WHITE[0]);
  screen->setPixel(1,2,WHITE[0]);
  screen->setPixel(1,3,WHITE[0]);
  screen->setPixel(2,2,WHITE[0]);
  screen->setPixel(2,3,WHITE[0]);
  screen->setPixel(3,3,WHITE[0]);

  EXPECT_PIXEL_IS_WHITE(1,1);
  EXPECT_PIXEL_IS_WHITE(1,2);
  EXPECT_PIXEL_IS_WHITE(1,3);
  EXPECT_PIXEL_IS_WHITE(2,2);
  EXPECT_PIXEL_IS_WHITE(2,3);
  EXPECT_PIXEL_IS_WHITE(3,3);
  WriteImage(image, "test_smallUpsideDownRightTriangle");
}

// Setting then Getting Pixel testing
TEST(SetPixelGetPixel, Random) {
  screen->clear();

  screen->setPixel(11,3,WHITE[0]);
  screen->setPixel(3,4,WHITE[0]);
  screen->setPixel(6,7,WHITE[0]);
  screen->setPixel(2,8,WHITE[0]);
  screen->setPixel(9,1,WHITE[0]);
  screen->setPixel(2,9,WHITE[0]);

  EXPECT_PIXEL_IS_WHITE(11,3);
  EXPECT_PIXEL_IS_WHITE(3,4);
  EXPECT_PIXEL_IS_WHITE(6,7);
  EXPECT_PIXEL_IS_WHITE(2,8);
  EXPECT_PIXEL_IS_WHITE(9,1);
  EXPECT_PIXEL_IS_WHITE(2,9);

  WriteImage(image, "test_randomPoints(11-3)(3-4)(6-7)(2-8)(9-1)(2-9)");
}

TEST(SetPixelGetPixel, Whiteout) {
  screen->clear();

  for (int y = 0; y < TEST_HEIGHT; y++) {
    for (int x = 0; x < TEST_WIDTH; x++) {
      screen->setPixel(x,y,WHITE[0]);
    }
  }

  for (int y = 0; y < TEST_HEIGHT; y++) {
    for (int x = 0; x < TEST_WIDTH; x++) {
      EXPECT_PIXEL_IS_WHITE(x,y);
    }
  }
}

TEST(ScreenClear, FromWhiteoutToClear) {
  //ARRANGE
  screen->clear();
  for (int y = 0; y < TEST_HEIGHT; y++) {
    for (int x = 0; x < TEST_WIDTH; x++) {
      screen->setPixel(x,y,WHITE[0]);
    }
  }
  for (int y = 0; y < TEST_HEIGHT; y++) {
    for (int x = 0; x < TEST_WIDTH; x++) {
      EXPECT_PIXEL_IS_WHITE(x,y);
    }
  }

  // ACT
  screen->clear();

  //ASSERT
  for (int y = 0; y < TEST_HEIGHT; y++) {
    for (int x = 0; x < TEST_WIDTH; x++) {
      EXPECT_PIXEL_IS_BLACK(x,y);
    }
  }
}

// Triangle Helpers
TEST(Triangle, RowMin1) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {1.0, 2.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(1, t.rowMin());
}

TEST(Triangle, RowMin2) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {0.9, 1.0, 1.1};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(1, t.rowMin());
}

TEST(Triangle, RowMin3) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {12345.0000000000001, 12344.9999999, 12345.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(12345, t.rowMin());
}

TEST(Triangle, RowMin4) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {10.001, 10230.99, 100};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(11, t.rowMin());
}

TEST(Triangle, RowMax1) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {1.0, 2.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(2, t.rowMax());
}

TEST(Triangle, RowMax2) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {0.9, 1.0, 1.1};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(1, t.rowMax());
}

TEST(Triangle, RowMax3) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {12345.0000000000001, 12344.9999999, 12345.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(12345, t.rowMax());
}

TEST(Triangle, RowMax4) {
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {10.001, 10230.99, 100};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  ASSERT_EQ(10230, t.rowMax());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ASSERT_BASIC_EQU_TRI(Triangle &t, unsigned int li, unsigned int ri, unsigned int ti) {
  ASSERT_FLOAT_EQ(1.0, t.X[li]);
  ASSERT_FLOAT_EQ(2.0, t.X[ti]);
  ASSERT_FLOAT_EQ(3.0, t.X[ri]);
}

TEST(Triangle, LeftRightTopIndicesEqu1) {
  //ARRANGE
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {1.0, 2.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_BASIC_EQU_TRI(t, li, ri, ti);
}

TEST(Triangle, LeftRightTopIndicesEqu2) {
  //ARRANGE
  double x[3] = {1.0, 3.0, 2.0};
  double y[3] = {1.0, 1.0, 2.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_BASIC_EQU_TRI(t, li, ri, ti);
}

TEST(Triangle, LeftRightTopIndicesEqu3) {
  //ARRANGE
  double x[3] = {2.0, 1.0, 3.0};
  double y[3] = {2.0, 1.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_BASIC_EQU_TRI(t, li, ri, ti);
}

TEST(Triangle, LeftRightTopIndicesEqu4) {
  //ARRANGE
  double x[3] = {2.0, 3.0, 1.0};
  double y[3] = {2.0, 1.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_BASIC_EQU_TRI(t, li, ri, ti);
}

TEST(Triangle, LeftRightTopIndicesEqu5) {
  //ARRANGE
  double x[3] = {3.0, 2.0, 1.0};
  double y[3] = {1.0, 2.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_BASIC_EQU_TRI(t, li, ri, ti);
}

TEST(Triangle, LeftRightTopIndicesEqu6) {
  //ARRANGE
  double x[3] = {3.0, 1.0, 2.0};
  double y[3] = {1.0, 1.0, 2.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_BASIC_EQU_TRI(t, li, ri, ti);
}

TEST(Triangle, LeftRightTopIndices_VerticalLine) {
  //ARRANGE
  double x[3] = {2.0, 2.0, 2.0};
  double y[3] = {1.0, 1.0, 2.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_FLOAT_EQ(1.0, t.Y[li]);
  ASSERT_FLOAT_EQ(1.0, t.Y[ri]);
  ASSERT_FLOAT_EQ(2.0, t.Y[ti]);
}

TEST(Triangle, LeftRightTopIndices_HorizontalLine) {
  //ARRANGE
  double x[3] = {10.0, 3.0, 1.0};
  double y[3] = {1.0, 1.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  unsigned int li, ri, ti;
  t.getLeftRightTop_flatBottom(li, ri, ti);

  // ASSERT
  ASSERT_FLOAT_EQ(1.0, t.X[li]);
  ASSERT_FLOAT_EQ(3.0, t.X[ti]);
  ASSERT_FLOAT_EQ(10.0, t.X[ri]);
}

// Rasterizing Misc Triangles
TEST(RasterizeTriangle, LargeTriangle1) {
  // ARRANGE
  screen->clear();
  double x[3] = {1.0, 9.0, 4.0};
  double y[3] = {1.0, 1.0, 5.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));
  EXPECT_PIXEL_IS_BLACK(0,1);
  EXPECT_PIXEL_IS_WHITE(1,1);
  EXPECT_PIXEL_IS_WHITE(2,1);
  EXPECT_PIXEL_IS_WHITE(3,1);
  EXPECT_PIXEL_IS_WHITE(4,1);
  EXPECT_PIXEL_IS_WHITE(5,1);
  EXPECT_PIXEL_IS_WHITE(6,1);
  EXPECT_PIXEL_IS_WHITE(7,1);
  EXPECT_PIXEL_IS_WHITE(8,1);
  EXPECT_PIXEL_IS_WHITE(9,1);
  EXPECT_PIXEL_IS_BLACK(10,1);

  EXPECT_PIXEL_IS_BLACK(2,3);
  EXPECT_PIXEL_IS_WHITE(3,3);
  EXPECT_PIXEL_IS_WHITE(4,3);
  EXPECT_PIXEL_IS_WHITE(5,3);
  EXPECT_PIXEL_IS_WHITE(6,3);
  EXPECT_PIXEL_IS_BLACK(7,3);

  EXPECT_PIXEL_IS_BLACK(3,5);
  EXPECT_PIXEL_IS_WHITE(4,5);
  EXPECT_PIXEL_IS_BLACK(5,5);

  WriteImage(image, "test_BigTriangle1");
}

// Rasterizing Misc Triangles
TEST(RasterizeTriangle, LargeTriangle2) {
  // ARRANGE
  screen->clear();
  double x[3] = {2.5, 2.5, 10.5};
  double y[3] = {1.5, 10.5, 1.5};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));
  EXPECT_PIXEL_IS_WHITE(3,5);
  EXPECT_PIXEL_IS_WHITE(4,5);
  EXPECT_PIXEL_IS_WHITE(5,5);
  EXPECT_PIXEL_IS_WHITE(6,5);
  EXPECT_PIXEL_IS_WHITE(7,5);

  EXPECT_PIXEL_IS_BLACK(2,9);
  EXPECT_PIXEL_IS_WHITE(3,9);
  EXPECT_PIXEL_IS_BLACK(4,9);

  WriteImage(image, "test_BigTriangle2");
}

TEST(RasterizeTriangle, SmallTriangle1) {
  // ARRANGE
  screen->clear();
  double x[3] = {1.0, 2.0, 3.0};
  double y[3] = {1.0, 2.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  EXPECT_PIXEL_IS_WHITE(1,1);
  EXPECT_PIXEL_IS_WHITE(2,1);
  EXPECT_PIXEL_IS_WHITE(3,1);
  EXPECT_PIXEL_IS_WHITE(2,2);
}

TEST(RasterizeTriangle, SmallTriangle2) {
  // ARRANGE
  screen->clear();
  double x[3] = {2.0, 1.0, 3.0};
  double y[3] = {2.0, 1.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  EXPECT_PIXEL_IS_WHITE(1,1);
  EXPECT_PIXEL_IS_WHITE(2,1);
  EXPECT_PIXEL_IS_WHITE(3,1);
  EXPECT_PIXEL_IS_WHITE(2,2);
  WriteImage(image, "test_FlatBottomEquTriangle");
}

TEST(RasterizeTriangle, SmallTriangle3) {
  // ARRANGE
  screen->clear();
  double x[3] = {3.0, 2.0, 1.0};
  double y[3] = {1.0, 2.0, 1.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  EXPECT_PIXEL_IS_WHITE(1,1);
  EXPECT_PIXEL_IS_WHITE(2,1);
  EXPECT_PIXEL_IS_WHITE(3,1);
  EXPECT_PIXEL_IS_WHITE(2,2);
}

TEST(RasterizeTriangle, NonFlatBottom1) {
  // ARRANGE
  screen->clear();
  double x[3] = {3.0, 2.0, 1.0};
  double y[3] = {2.0, 1.0, 3.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  EXPECT_PIXEL_IS_BLACK(1,1);
  EXPECT_PIXEL_IS_WHITE(2,1);
  EXPECT_PIXEL_IS_BLACK(3,1);

  EXPECT_PIXEL_IS_BLACK(1,2);
  EXPECT_PIXEL_IS_WHITE(2,2);
  EXPECT_PIXEL_IS_WHITE(3,2);
  EXPECT_PIXEL_IS_BLACK(4,2);

  EXPECT_PIXEL_IS_BLACK(0,3);
  EXPECT_PIXEL_IS_WHITE(1,3); 
  EXPECT_PIXEL_IS_BLACK(2,3);
  
  WriteImage(image, "test_nonFlatBottom1");
}

TEST(RasterizeTriangle, NonFlatBottom2) {
  // ARRANGE
  screen->clear();
  double x[3] = {7.17, 7.14, 5.86};
  double y[3] = {7.94, 9.25, 7.91};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  EXPECT_PIXEL_IS_BLACK(5,8);
  EXPECT_PIXEL_IS_WHITE(6,8);
  EXPECT_PIXEL_IS_WHITE(7,8);
  EXPECT_PIXEL_IS_BLACK(8,8);

  EXPECT_PIXEL_IS_BLACK(6,9);
  EXPECT_PIXEL_IS_WHITE(7,9);
  EXPECT_PIXEL_IS_BLACK(8,9);
  
  // WriteImage(image, "test_nonFlatBottom2");
}

TEST(RasterizeTriangle, NonFlatBottom3) {
  // ARRANGE
  screen->clear();
  double x[3] = {1.0, 5.0, 10.0};
  double y[3] = {6.0, 2.0, 9.0};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  // Top Row
  EXPECT_PIXEL_IS_BLACK(9, 9);
  EXPECT_PIXEL_IS_WHITE(10,9);
  EXPECT_PIXEL_IS_BLACK(11,9);

  // Middle Row
  EXPECT_PIXEL_IS_BLACK(0,6);
  EXPECT_PIXEL_IS_WHITE(1,6);
  EXPECT_PIXEL_IS_WHITE(7,6);
  EXPECT_PIXEL_IS_BLACK(8,6);

  // Bottom Row
  EXPECT_PIXEL_IS_BLACK(4,2);
  EXPECT_PIXEL_IS_WHITE(5,2);
  EXPECT_PIXEL_IS_BLACK(6,2);
  
  // WriteImage(image, "test_nonFlatBottom3");
}


TEST(RasterizeTriangle, Offscreen) {
  //(47.0926, -1.7645) (48.4026, -0.454498) (47.0698, -0.454498)
  // ARRANGE
  screen->clear();
  double x[3] = {7.0926, 8.4026, 7.0698};
  double y[3] = {-1.7645, -0.454498, -0.454498};
  double z[3] = {0.0, 0.0, 0.0};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), WHITE);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  //ASSERT
  for (int y = 0; y < TEST_HEIGHT; y++) {
    for (int x = 0; x < TEST_WIDTH; x++) {
      EXPECT_PIXEL_IS_BLACK(x,y);
    }
  }
  
  WriteImage(image, "test_offscreen1");
}

TEST(Point, lerp_x) {
  // ACT
  Point res = lerp(Point(1.0,1.0,0.0), Point(6.0,1.0,0.0), 0.5);

  // ASSERT
  ASSERT_FLOAT_EQ(1.0, res.y);
  ASSERT_FLOAT_EQ(3.5, res.x);
  ASSERT_FLOAT_EQ(0.0, res.z);
}

TEST(Point, lerp_x2) {
  // ACT
  Point res = lerp(Point(6.0,1.0,0.0), Point(1.0,1.0,0.0), 0.5);

  // ASSERT
  ASSERT_FLOAT_EQ(1.0, res.y);
  ASSERT_FLOAT_EQ(3.5, res.x);
  ASSERT_FLOAT_EQ(0.0, res.z);
}

TEST(Point, lerp_y) {
  // ACT
  Point res = lerp(Point(1.0,1.0,0.0), Point(1.0,6.0,0.0), 0.5);

  // ASSERT
  ASSERT_FLOAT_EQ(1.0, res.x);
  ASSERT_FLOAT_EQ(3.5, res.y);
  ASSERT_FLOAT_EQ(0.0, res.z);
}

TEST(Point, lerp_y2) {
  // ACT
  Point res = lerp(Point(1.0,6.0,0.0), Point(1.0,1.0,0.0), 0.5);

  // ASSERT
  ASSERT_FLOAT_EQ(1.0, res.x);
  ASSERT_FLOAT_EQ(3.5, res.y);
  ASSERT_FLOAT_EQ(0.0, res.z);
}

TEST(Point, lerp_diag) {
  // ACT
  Point res = lerp(Point(1.0,1.0,0.0), Point(6.0,6.0,0.0), 0.5);

  // ASSERT
  ASSERT_FLOAT_EQ(3.5, res.x);
  ASSERT_FLOAT_EQ(3.5, res.y);
  ASSERT_FLOAT_EQ(0.0, res.z);
}

TEST(Point, lerpy_diag) {
  // ACT
  // 187.6
  // -103
  // (187/-103)*0.5
  Point res = lerpy(Point(1.0,1.0,0.0), Point(6.0,6.0,0.0), 3.5);

  // ASSERT
  ASSERT_FLOAT_EQ(3.5, res.x);
  ASSERT_FLOAT_EQ(3.5, res.y);
  ASSERT_FLOAT_EQ(0.0, res.z);
}

TEST(Point, lerpy_real1) {
  // ACT
  // 187.6
  // -103
  // (187/-103)*0.5
  Point res = lerpy(Point(5.86,7.91,0.0), Point(7.14,9.25,0.0),  7.94);

  // ASSERT
  ASSERT_FLOAT_EQ(7.94, res.y);
  ASSERT_TRUE(res.x > 5.86 && res.x < 7.14);
  ASSERT_FLOAT_EQ(5.8886566, res.x);
  ASSERT_FLOAT_EQ(0.0, res.z);
}

TEST(Point, lerpy_real2) {
  // ACT
  // 187.6
  // -103
  // (187/-103)*0.5
  Point res = lerpy(Point(7.14,9.25,0.0), Point(5.86,7.91,0.0), 7.94);

  // ASSERT
  ASSERT_FLOAT_EQ(7.94, res.y);
  ASSERT_TRUE(res.x > 5.86 && res.x < 7.14);
  ASSERT_FLOAT_EQ(5.8886566, res.x);
  ASSERT_FLOAT_EQ(0.0, res.z);
}


TEST(RasterizeTriangle, NewColors1) {
  // ARRANGE
  double RED[3] = {1.0,0.0,0.0};
  double GRN[3] = {0.0,1.0,0.0};
  double BLU[3] = {0.0,0.0,1.0};
  screen->clear();
  double x[3] = {1,2,3};
  double y[3] = {1,2,1};
  double z[3] = {0,0,0};
  double colors[3][3] = {{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}};

  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), colors);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  double PURPLE[3] = {0.5,0.5,0.5};
  EXPECT_PIXEL_IS_COLOR(1, 1, RED);
  EXPECT_PIXEL_IS_COLOR(2, 2, GRN);
  EXPECT_PIXEL_IS_COLOR(3, 1, BLU);
  // EXPECT_PIXEL_IS_COLOR(2, 1, PURPLE);

  WriteImage(image, "test_newColors1");
}

TEST(RasterizeTriangle, NewColors2) {
  // ARRANGE
  double RED[3] = {1.0,0.0,0.0};
  double GRN[3] = {0.0,1.0,0.0};
  double BLU[3] = {0.0,0.0,1.0};
  screen->clear();
  double x[3] = {0,15,30};
  double y[3] = {0,15,0};
  double z[3] = {0,0,0};
  double colors[3][3] = {{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.0,1.0}};

  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), colors);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  double PURPLE[3] = {0.5,0.5,0.5};
  EXPECT_PIXEL_IS_COLOR(0, 0, RED);
  EXPECT_PIXEL_IS_COLOR(15, 15, GRN);
  EXPECT_PIXEL_IS_COLOR(30, 0, BLU);
  // EXPECT_PIXEL_IS_COLOR(2, 1, PURPLE);

  WriteImage(image, "test_newColors2");
}

TEST(RasterizeTriangle, NewColors3) {
  // ARRANGE
  screen->clear();
  double x[3] = {1.809, 3.316, 0.744};
  double y[3] = {7.143, 7.143, 27.551};
  double z[3] = {-0.734694, -0.714286, -0.734694};
  double colors[3][3] = {{0.404742, 0.703538, 0}, {0.384941, 0.693677, 0}, {0.450988, 0.726571, 0}};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), colors);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  // TODO
  WriteImage(image, "test_newColors3");
}

TEST(RasterizeTriangle, NewColors4) {
  // ARRANGE
  screen->clear();
  double x[3] = {1, 10, 20};
  double y[3] = {1, 6, 6};
  double z[3] = {0,0,0};
  double colors[3][3] = {{0.404742, 0.703538, 0}, {0.384941, 0.693677, 0}, {0.450988, 0.726571, 0}};
  Triangle t(Point(x, y, z), Point(0.0,0.0,-1.0), colors);

  // ACT
  ASSERT_TRUE(t.rasterize(*screen, ambient_lp));

  // ASSERT
  // TODO
  WriteImage(image, "test_newColors4");
}

TEST(Triangle, Printing) {
  std::vector<Triangle> triangles = GetTriangles("res/proj1d_geometry.vtk");
  std::cout << triangles[12345] << std::endl;
}
*/
void ASSERT_VECTOR_EQUAL(double x, double y, double z, Vector &observed) {
  ASSERT_FLOAT_EQ(observed.x, x);
  ASSERT_FLOAT_EQ(observed.y, y);
  ASSERT_FLOAT_EQ(observed.z, z);
}

void ASSERT_VECTOR_EQUAL(Vector &expected, Vector &observed) {
  ASSERT_FLOAT_EQ(expected.x, observed.x);
  ASSERT_FLOAT_EQ(expected.y, observed.y);
  ASSERT_FLOAT_EQ(expected.z, observed.z);
}

TEST(Vector, Constructor1) {
  //ACT
  Vector v;

  // ASSERT
  ASSERT_VECTOR_EQUAL(0.0,0.0,0.0,v);
}

TEST(Vector, Constructor2) {
  //ACT
  Vector v(0.0, 1.0, 2.0);

  // ASSERT
  ASSERT_VECTOR_EQUAL(0.0,1.0,2.0,v);
}

TEST(Vector, Constructor3) {
  //ACT
  Vector v({0.0, 1.0, 2.0});

  // ASSERT
  ASSERT_VECTOR_EQUAL(0.0,1.0,2.0,v);
}

TEST(Vector, CopyConstructor) {
  // ARRANGE
  Vector v1(0.0, 1.0, 2.0);

  //ACT
  Vector v2 = v1;

  // ASSERT
  ASSERT_VECTOR_EQUAL(v1,v2);
}

TEST(Vector, Dot1) {
  Vector v1(1.0, 1.0, 1.0);
  Vector v2(5.0, 3.0, 1.0);

  //ACT & ASSERT
  ASSERT_FLOAT_EQ(9.0, Vector::Dot(v1,v2));
}

TEST(Vector, Dot2) {
  Vector v1(0.0, 0.0, 0.0);
  Vector v2(5.0, 3.0, 1.0);

  //ACT & ASSERT
  ASSERT_FLOAT_EQ(0.0, Vector::Dot(v1,v2));
}

TEST(Vector, Dot3) {
  Vector v1(0.0, 0.0, 0.0);
  Vector v2(5.0, 3.0, 1.0);

  //ACT & ASSERT
  ASSERT_FLOAT_EQ(0.0, Vector::Dot(v2, v1));
}

TEST(Vector, Dot4) {
  Vector v1(-0.3, 0.0, -0.7);
  Vector v2(0.6, 0.0, 0.8);

  //ACT & ASSERT
  ASSERT_FLOAT_EQ(-0.74, Vector::Dot(v2, v1));
}

TEST(Point, lerp_shareSameNormal_shouldLerpToSameNormal) {
  Vector n(0.0, 1.0, 0.0);
  Point p1(0.0,0.0,0.0, WHITE[0], n);
  Point p2(1.0,0.0,0.0, WHITE[0], n);

  Point res = Point::lerp(p1,p2, 0.5);

  ASSERT_VECTOR_EQUAL(n, res.normal);
}

TEST(Point, lerp_shareSamePoint_shouldLerpToSameNormal) {
  Vector n(0.0, 0.0, 1.0);
  Point p1(1.0,0.0,0.0, WHITE[0], n);
  Point p2(1.0,0.0,0.0, WHITE[0], n);

  Point res1 = Point::lerp(p1,p2, 1.0);
  Point res2 = Point::lerp(p1,p2, 0.5);
  Point res3 = Point::lerp(p1,p2, 0.0);

  ASSERT_VECTOR_EQUAL(n, res1.normal);
  ASSERT_VECTOR_EQUAL(n, res2.normal);
  ASSERT_VECTOR_EQUAL(n, res3.normal);
}

TEST(Point, lerp_) {
  Vector n(0.0, 0.0, 1.0);
  Point p1(1.0,0.0,0.0, WHITE[0], n);
  Point p2(1.0,0.0,0.0, WHITE[0], n);

  Point res1 = Point::lerp(p1,p2, 1.0);
  Point res2 = Point::lerp(p1,p2, 0.5);
  Point res3 = Point::lerp(p1,p2, 0.0);

  ASSERT_VECTOR_EQUAL(n, res1.normal);
  ASSERT_VECTOR_EQUAL(n, res2.normal);
  ASSERT_VECTOR_EQUAL(n, res3.normal);
}

TEST(Point, lerp_oppositeNormals_shouldTBD) {
  // Opposite ==> the zero vector is the only sensable return vector.
  Vector n1(0.0, -1.0, 0.0);
  Vector n2(0.0, 1.0, 0.0);
  Vector expected_n(0.0, 0.0, 0.0);
  Point p1(0.0,0.0,0.0, WHITE[0], n1);
  Point p2(1.0,0.0,0.0, WHITE[0], n2);

  Point res = Point::lerp(p1,p2, 0.5);

  ASSERT_VECTOR_EQUAL(expected_n, res.normal);
}

TEST(Point, lerp_halfwayLerpBetweenXandYnormal_shouldGetNormalBetween) {
  Vector n1(1.0, 0.0, 0.0);
  Vector n2(0.0, 1.0, 0.0);
  Vector expected_n(0.7071067710677, 0.7071067710677, 0.0);
  Point p1(0.0,0.0,0.0, WHITE[0], n1);
  Point p2(1.0,0.0,0.0, WHITE[0], n2);

  Point res = Point::lerp(p1,p2, 0.5);

  ASSERT_VECTOR_EQUAL(expected_n, res.normal);
}

TEST(Point, lerp) {
  Vector n1(-1.0, 0.0, 0.0);
  Vector n2(0.0, 1.0, 0.0);
  Vector expected_n(-0.7071067710677, 0.7071067710677, 0.0);
  Point p1(0.0,0.0,0.0, WHITE[0], n1);
  Point p2(1.0,0.0,0.0, WHITE[0], n2);

  Point res = Point::lerp(p1,p2, 0.5);

  ASSERT_VECTOR_EQUAL(expected_n, res.normal);
}

TEST(Camera, GetCamera_0_1000) {
  //ACT
  Camera c = Camera::GetCamera(0,1000);

  //ASSERT
  ASSERT_FLOAT_EQ(5,c.near);
  ASSERT_FLOAT_EQ(200,c.far);
  ASSERT_FLOAT_EQ(M_PI/6,c.angle);
  ASSERT_FLOAT_EQ(0, c.position[0]);
  ASSERT_FLOAT_EQ(40, c.position[1]);
  ASSERT_FLOAT_EQ(40, c.position[2]);
  ASSERT_FLOAT_EQ(0, c.focus[0]);
  ASSERT_FLOAT_EQ(0, c.focus[1]);
  ASSERT_FLOAT_EQ(0, c.focus[2]);
  ASSERT_FLOAT_EQ(0, c.up[0]);
  ASSERT_FLOAT_EQ(1, c.up[1]);
  ASSERT_FLOAT_EQ(0, c.up[2]);
}

void ASSERT_MATRIX_EQ(double expected[16], Matrix observed) {
  for (int y = 0; y < 4; y++) {
    for (int x = 0; x < 4; x++)  {
      EXPECT_FLOAT_EQ(expected[x+(y*4)], observed.get(x,y)) << "x: " << x << std::endl << "y: " << y; 
    }
  }
}

TEST(Test, AssertMatrixEq) {
  Matrix x;
  x.set(0,0,1);
  x.set(1,0,2);
  x.set(2,0,3);
  x.set(3,0,4);
  x.set(0,1,11);
  x.set(1,1,22);
  x.set(2,1,33);
  x.set(3,1,44);
  x.set(0,2,111);
  x.set(1,2,222);
  x.set(2,2,333);
  x.set(3,2,444);
  x.set(0,3,1111);
  x.set(1,3,2222);
  x.set(2,3,3333);
  x.set(3,3,4444);
  double expected[16] = {1,2,3,4,11,22,33,44,111,222,333,444,1111,2222,3333,4444};
  ASSERT_MATRIX_EQ(expected, x);

}

/* TODO 
TEST(Utility, cotangent1) {
  double ret = cot(90.0);
  ASSERT_FLOAT_EQ(0, ret);
}
*/

TEST(Utility, cotangent3) {
  double ret = cot(1.5);
  ASSERT_FLOAT_EQ(0.070914842, ret);
}

TEST(Utility, cotangent2) {
  double ret = cot(0.5);
  ASSERT_FLOAT_EQ(1.8304877, ret);
}

TEST(Vector, CrossProduct1) {
  Vector v1(0.0,0.0,0.0);
  Vector v2(0.0,0.0,0.0);
  Vector r = Vector::Cross(v1,v2);
  ASSERT_VECTOR_EQUAL(0.0,0.0,0.0, r);
}

TEST(Vector, CrossProduct2) {
  Vector v1(1.0,1.0,1.0);
  Vector v2(0.0,0.0,0.0);
  Vector r = Vector::Cross(v1,v2);
  ASSERT_VECTOR_EQUAL(0.0,0.0,0.0, r);
}

TEST(Vector, CrossProduct4) {
  Vector v1(2.0,3.0,4.0);
  Vector v2(5.0,6.0,7.0);
  Vector r = Vector::Cross(v1,v2);
  ASSERT_VECTOR_EQUAL(-3.0,6.0,-3.0, r);
}

TEST(Camera, View_Transform_GetCamera_0_1000) {
  Camera c = Camera::GetCamera(0,1000);

  //ACT
  Matrix t = c.ViewTransform();

  //ASSERT
  double expected[16] = 
	{3.7320509, 0, 0, 0,
	 0, 3.7320509, 0, 0,
	 0, 0, 1.051282, -1,
	 0, 0, 10.256411, 0};
  ASSERT_MATRIX_EQ(expected, t);
  t.Print(cout);
}

TEST(Camera, Camera_Transform_GetCamera_0_1000) {
  Camera c = Camera::GetCamera(0,1000);

  //ACT
  Matrix t = c.CameraTransform();

  //ASSERT
  double expected[16] = 
	{1, 0, 0, 0,
	 0, 0.70710677, 0.70710677, 0,
	 0, -0.70710677, 0.70710677, 0,
	 0, 0, -56.568542, 1};
  ASSERT_MATRIX_EQ(expected, t);
  t.Print(cout);
}

TEST(Camera, Device_Transform_GetCamera_0_1000) {
  Camera c = Camera::GetCamera(0,1000);

  //ACT
  Matrix t = c.DeviceTransform(1000,1000);

  //ASSERT
  double expected[16] = 
	{500, 0, 0, 0,
	 0, 500, 0, 0,
	 0, 0, 1, 0,
	 500, 500, 0, 1};
  ASSERT_MATRIX_EQ(expected, t);
  t.Print(cout);
}

TEST(Camera, Complete_Transform_GetCamera_0_1000) {
  Camera c = Camera::GetCamera(0,1000);
  Matrix ct = c.CameraTransform();
  Matrix vt = c.ViewTransform();
  Matrix dt = c.DeviceTransform(1000,1000);

  //ACT
  Matrix cvt = Matrix::ComposeMatrices(ct, vt);
  Matrix fin = Matrix::ComposeMatrices(cvt, dt);

  //ASSERT
  double expected[16] = 
	{1866.0254038, 0.0000000, 0.0000000, 0.0000000,
  	-353.5533906, 965.9258263, 0.7433687, -0.7071068,
  	-353.5533906, -1673.0326075, 0.7433687, -0.7071068,
  	28284.2712475, 28284.2712475, -49.2130831, 56.5685425};

  ASSERT_MATRIX_EQ(expected, fin);
  fin.Print(cout);
}


int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);

  // Setup main fixture stufff
  // Going to have a single screen through all tests, and just write to it then call 'clear' on it
  image = NewImage(TEST_WIDTH, TEST_HEIGHT);
  ClearImage(TEST_WIDTH, TEST_HEIGHT, image);
  unsigned char *buffer = (unsigned char *) image->GetScalarPointer(0,0,0);
  screen = new Screen(TEST_WIDTH, TEST_HEIGHT, buffer);

  int ret = RUN_ALL_TESTS();
  delete screen;
}
