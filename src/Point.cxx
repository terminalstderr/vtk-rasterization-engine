/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: "Point" Helper 
 */
#include <glog/logging.h>

#include "Point.h"
#include "Vector.h"
#include "Utility.h"

Point::Point(const Vector &position, const Vector &normal, const double shading,  double red, double grn, double blue) : 
  position(position),
  normal(normal), 
  shading(shading),
  color {red, grn, blue} 
  {}

Point::Point(const Vector &position, const Vector &normal, const double shading, const double color[3]) :
    Point(position, normal, shading, color[RED_CH], color[GRN_CH], color[BLU_CH])
  {}

Point::Point() :
  Point(Vector(), Vector(), 0.0, 0.0, 0.0, 0.0) {}

Point Point::operator-(const Point &rhs) {
  return Point(
      this->position - rhs.position, 
      this->normal - rhs.normal, 
      this->shading - rhs.shading,
      this->color[RED_CH] - rhs.color[RED_CH], 
      this->color[GRN_CH] - rhs.color[GRN_CH],  
      this->color[BLU_CH] - rhs.color[BLU_CH]);
}

/**
 * Alpha must be between 0 and 1
 */
Point Point::lerp(Point p1, Point p2, double alpha) {
  Point diff = p1 - p2;
  Vector interp_normal(p2.normal.x + (diff.normal.x*alpha), p2.normal.y + (diff.normal.y*alpha), p2.normal.z + (diff.normal.z*alpha));
  // If we have two normals that are exact opposites, we end up with the zero vector...
  if  (interp_normal.x == 0 && interp_normal.y == 0 && interp_normal.z == 0) 
    LOG(WARNING) << "Have a zero normal! ";
  // Normalize the interpolated normal vector
  interp_normal.normalize();
  return Point(
		Vector(p2.position.x + (diff.position.x*alpha),
		  p2.position.y + (diff.position.y*alpha),
		  p2.position.z + (diff.position.z*alpha)),
		interp_normal,
		p2.shading + (diff.shading*alpha),
		p2.color[RED_CH] + (diff.color[RED_CH]*alpha),
		p2.color[GRN_CH] + (diff.color[GRN_CH]*alpha),
		p2.color[BLU_CH] + (diff.color[BLU_CH]*alpha)
	      );
}

Point Point::lerpy(Point p1, Point p2, double y_val) {
  double alpha;
  if ((p2.position.y - p1.position.y) == 0.0) {
    alpha = 1.0;
  } else {
    alpha = (p2.position.y - y_val)/(p2.position.y - p1.position.y);
  }
  return lerp(p1, p2, alpha);
}

Point Point::lerpx(Point p1, Point p2, double x_val) {
  double alpha;
  if ((p2.position.x - p1.position.x) == 0.0) {
    alpha = 1.0;
  } else {
    alpha = (p2.position.x - x_val)/(p2.position.x - p1.position.x);
  }
  return lerp(p1, p2, alpha);
}

