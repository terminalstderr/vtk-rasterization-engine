/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: 2D Triangle Primitive
 */
#include <stdexcept>
#include <float.h>
#include <glog/logging.h>
#include "Triangle.h"
#include "Utility.h"
#include "Screen.h"
#include "Point.h"



Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3, unsigned int id) :
  id (id),
  X {p1.position.x, p2.position.x, p3.position.x},
  Y {p1.position.y, p2.position.y, p3.position.y},
  Z {p1.position.z, p2.position.z, p3.position.z},
  colors {{p1.color[RED_CH], p1.color[GRN_CH], p1.color[BLU_CH]},
	  {p2.color[RED_CH], p2.color[GRN_CH], p2.color[BLU_CH]}, 
	  {p3.color[RED_CH], p3.color[GRN_CH], p3.color[BLU_CH]}
	 },
  normals{{p1.normal.x, p1.normal.y, p1.normal.z},
	  {p2.normal.x, p2.normal.y, p2.normal.z}, 
	  {p3.normal.x, p3.normal.y, p3.normal.z}
	 },
  shading{p1.shading, p2.shading, p3.shading}
  {}

Triangle::Triangle() : 
  Triangle(Point(), Point(), Point(), 0) 
  {}

// Can define this as a general function since all of the triangle fields are public
std::ostream& operator<<(std::ostream &out, const Triangle &t) {
  // [0](x,y,z) (255,234,210) -- [1](x,y,z) (255,234,210) -- [2](x,y,z) (255,234,210)
  out << "TRI: " << t.id
  << " -- [0](" << t.X[0] << ", " << t.Y[0] << ", " << t.Z[0] << ") RGB: " << "(" << t.colors[0][0] << ", " << t.colors[0][1] << ", " << t.colors[0][2] << ")"
  << " -- [1](" << t.X[1] << ", " << t.Y[1] << ", " << t.Z[1] << ") RGB: " << "(" << t.colors[1][0] << ", " << t.colors[1][1] << ", " << t.colors[1][2] << ")" 
  << " -- [2](" << t.X[2] << ", " << t.Y[2] << ", " << t.Z[2] << ") RGB: " << "(" << t.colors[2][0] << ", " << t.colors[2][1] << ", " << t.colors[2][2] << ")";
    
  return out;
}

// This must handle rasterizing (drawing) the triangle to the screen regardless of what type of triangle it is.
bool Triangle::rasterize(Screen &screen) {
  // If flat bottom or flat top -- handle 
  unsigned int miny_i; 
  unsigned int maxy_i; 
  unsigned int midy_i; 
  getBottomTopMid(miny_i, maxy_i, midy_i);

  // Flat Bottom
  // Check if the miny is equal to the 'midy'
  bool ret1 = false;
  bool ret2 = false;
  if (Y[miny_i] == Y[midy_i]) {
    ret1 = this->rasterize_flatBottom(screen);
  } else if (Y[maxy_i] == Y[midy_i]) {
    ret1 = this->rasterize_flatTop(screen);
  } else {
    // Split the triangle!
    /* This split is with respect to 'y'
     *
     * Figure 1: Example Original Triangle (assume that the edges are flat)
     *		      . <- max_vertex
     *		      |\
     *		      | \
     *		      |  \
     *	 mid_vertex-> .   |
     *		      \   |
     *		       \  \
     *		        \  |
     *		         \ |
     *		          \|
     *		           . <- min_vertex
     *
     * Figure 1: Example Triangle Split into two triangles (assume that the edges are flat)
     *		      . <- max_vertex
     *		      |\
     *		      | \ <------------------Top Triangle
     *		      |  \
     *	 mid_vertex-> .___. <-split_vertex
     *		      \   |
     *		       \  \
     *		        \  | <---------------Bottom Triangle
     *		         \ |
     *		          \|
     *		           . <- min_vertex
     */
    //
    Point min_vertex(Vector(X[miny_i], Y[miny_i], Z[miny_i]), normals[miny_i], shading[miny_i], colors[miny_i]);
    Point mid_vertex(Vector(X[midy_i], Y[midy_i], Z[midy_i]), normals[midy_i], shading[midy_i], colors[midy_i]);
    Point max_vertex(Vector(X[maxy_i], Y[maxy_i], Z[maxy_i]), normals[maxy_i], shading[maxy_i], colors[maxy_i]);
    Point split_vertex = Point::lerpy(min_vertex, max_vertex, Y[midy_i]);
    // We set the split vertex y-value to avoid floating point precision issues
    split_vertex.position.y = mid_vertex.position.y;
    // Bottom Triangle
    ret1 = Triangle(min_vertex, mid_vertex, split_vertex, this->id).rasterize_flatTop(screen);
    // Top Triangle
    ret2 = Triangle(max_vertex, mid_vertex, split_vertex, this->id).rasterize_flatBottom(screen);
  }
  return ret1 && ret2;
}

double Triangle::rowMin() {
  return ceil441 (std::min(Y[2], std::min(Y[0],Y[1])));
}

double Triangle::rowMax() {
  return floor441 (std::max(Y[2], std::max(Y[0],Y[1])));
}

// This method exclusively looks at the 'Y' values of this triangle.
void Triangle::getBottomTopMid(unsigned int &min_y_index, unsigned int &max_y_index, unsigned int &mid_y_index) {
  double min_y = DBL_MAX;
  double max_y = -DBL_MAX;
  for (int i = 0; i < POINTS_PER_TRIANGLE; i++) {
    if (Y[i] <= min_y) {
      min_y_index = i;
      min_y = Y[i];
    }
    if (Y[i] >= max_y) {
      max_y_index = i;
      max_y = Y[i];
    }
  }
  mid_y_index = getRemainingIndex(min_y_index, max_y_index);
}

// The returned index is figured using this mapping
// sum(i1, i2) == 3 ==> 0
// sum(i1, i2) == 2 ==> 1
// sum(i1, i2) == 1 ==> 2
unsigned int Triangle::getRemainingIndex(const unsigned int i1, const unsigned int i2) {
  switch (i1 + i2) {
    case 3:
      return 0;
    case 2:
      return 1;
    case 1:
      return 2;
    default:
      LOG(WARNING) << "Remaining Index could not be calculated (" << i1 << ","  << i2 << ")";
      return 0;
  }
}

// This method assumes that this triangle has a flat bottom
bool Triangle::getLeftRightBottom_flatTop(unsigned int &left_index, unsigned int &right_index, unsigned int &bottom_index) {
  // Find two that share the minimum 'y'
  double max_y = std::max(Y[2], std::max(Y[0],Y[1]));
  for (int a = 0, b = 1, c = 2; a < POINTS_PER_TRIANGLE; a++, b = (b + 1) % POINTS_PER_TRIANGLE, c = (c + 1) % POINTS_PER_TRIANGLE) {
    if (Y[a] == Y[b] && Y[a] == max_y) { // implies we have a 'flat bottom'

      // Special case, if all are equal then we have a flat line
      if (Y[c] == max_y) {
	// Need to find the minX and maxX -- can be done in one loop
	double min_x = DBL_MAX;
	double max_x = -DBL_MAX;
	for (unsigned int i = 0; i < POINTS_PER_TRIANGLE; i++) {
	  if (X[i] <= min_x) {
	    min_x = X[i];
	    left_index = i;
	  }
	  if (X[i] >= max_x) {
	    max_x = X[i];
	    right_index = i;
	  }
	}
	bottom_index = getRemainingIndex(left_index, right_index);
	return true;
      } 
      // End the special horizontal line case, begin the basic case handling
      else { 
	if (X[a] > X[b]) {
	  left_index = b;
	  right_index = a;
	  bottom_index = c;
	  return true;
	} else if (X[a] <= X[b]) { // handles the 'vertical line' case since it has the '<=' operator, e.g. if X[a] == X[b] 
	  left_index = a;
	  right_index = b;
	  bottom_index = c;
	  return true;
	}     
      }
    } 
  }
  // If this triangle was not flat topped, it will definitely get here
  LOG(WARNING) << "Unable to determine left and right indices of triangle " << *this;
  return false;
}


// TODO: Flat Top is almost identical to flat bottom -- except for the one helper call...
bool Triangle::rasterize_flatTop(Screen &screen) {
  // Parse the triangle into lower-left, lower-right, and top
  unsigned int l_i, r_i, b_i;
  if (!getLeftRightBottom_flatTop(l_i, r_i, b_i)) {
    LOG(WARNING) << "Could not rasterize flat topped triangle " << *this;
    return false;
  }

  // Now need to run the scanline algorithm on this triangle
  scanline(screen, l_i, r_i, b_i);
  return true;
}

// This method assumes that this triangle has a flat bottom
bool Triangle::getLeftRightTop_flatBottom(unsigned int &left_index, unsigned int &right_index, unsigned int &top_index) {
  // Find two that share the minimum 'y'
  double min_y = std::min(Y[2], std::min(Y[0],Y[1]));
  for (int a = 0, b = 1, c = 2; a < POINTS_PER_TRIANGLE; a++, b = (b + 1) % POINTS_PER_TRIANGLE, c = (c + 1) % POINTS_PER_TRIANGLE) {
    if (Y[a] == Y[b] && Y[a] == min_y) { // implies we have a 'flat bottom'

      // Special case, if all are equal then we have a flat line
      if (Y[c] == min_y) {
	// Need to find the minX and maxX -- can be done in one loop
	double min_x = DBL_MAX;
	double max_x = -DBL_MAX;
	for (unsigned int i = 0; i < POINTS_PER_TRIANGLE; i++) {
	  if (X[i] <= min_x) {
	    min_x = X[i];
	    left_index = i;
	  }
	  if (X[i] >= max_x) {
	    max_x = X[i];
	    right_index = i;
	  }
	}
	top_index = getRemainingIndex(left_index, right_index);
	return true;
      } 
      // End the special horizontal line case, begin the basic case handling
      else { 
	if (X[a] > X[b]) {
	  left_index = b;
	  right_index = a;
	  top_index = c;
	  return true;
	} else if (X[a] <= X[b]) { // handles the 'vertical line' case since it has the '<=' operator, e.g. if X[a] == X[b] 
	  left_index = a;
	  right_index = b;
	  top_index = c;
	  return true;
	}     
      }
    } 
  }
  // If this triangle was not flat bottomed, it will definitely get here
  LOG(WARNING) << "Unable to determine left and right indices of triangle " << *this;
  return false;
}

bool Triangle::rasterize_flatBottom(Screen &screen) {
  // Parse the triangle into lower-left, lower-right, and top
  unsigned int l_i, r_i, t_i;
  if (!getLeftRightTop_flatBottom(l_i, r_i, t_i)) {
    LOG(WARNING) << "Could not rasterize flat bottomed triangle " << *this;
    return false;
  }

  // Now need to run the scanline algorithm on this triangle
  scanline(screen, l_i, r_i, t_i);
  return true;
}

void Triangle::scanline(Screen &screen, unsigned int left_i, unsigned int right_i, unsigned int bottom_top_i) {
  // Now need to run the scanline algorithm on this triangle
  for (double row_y = rowMin(); row_y <= rowMax(); row_y++) {
    // Put into 'point' form for easy interaction
    Point left(Vector(X[left_i], Y[left_i], Z[left_i]), Vector(normals[left_i]), shading[left_i], colors[left_i]);
    Point right(Vector(X[right_i], Y[right_i], Z[right_i]), Vector(normals[right_i]), shading[right_i], colors[right_i]);
    Point bottom_top(Vector(X[bottom_top_i], Y[bottom_top_i], Z[bottom_top_i]), Vector(normals[bottom_top_i]), shading[bottom_top_i], colors[bottom_top_i]);
    
    // Lerp between the y values.
    Point left_end = Point::lerpy(left, bottom_top, row_y);
    Point right_end = Point::lerpy(right, bottom_top, row_y);

    for (int column_x = ceil441(left_end.position.x); column_x <= floor441(right_end.position.x); column_x++) {
      Point fragment = Point::lerpx(left_end, right_end, column_x);
      if (fragment.position.z > 0.0 && fragment.position.z < -1.0) {
	LOG(WARNING) << "Some Interpolated bits went out of zbuffer range" << *this;
	return;
      }
      if (fragment.position.z > screen.getZ(column_x, row_y)) {
	double shaded_color[3];
	VectorScalarMultiplyMax1(fragment.color, fragment.shading, shaded_color);
	screen.setPixel(column_x, row_y, shaded_color);
	screen.setZ(column_x, row_y, fragment.position.z);
	screen.setTriangleId(column_x, row_y, this->id);
      }
    }
  }
}
