/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Utility functions
 */
#include <glog/logging.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkCellArray.h>
#include <vtkPNGWriter.h>
#include <vtkDataSetWriter.h>
#include <vector>
#include <cmath>


#include "Utility.h"
#include "Triangle.h"
#include "Vector.h"
#include "Point.h"
#include "Matrix.h"

double angle_to_radian(double angle) {
  return (angle * M_PI) / 180;
}

double cot(double angle_rad) {
  double tan_val = std::tan(angle_rad);
  if (tan_val == 0.0)
    return 0.0;
  return 1 / tan_val;
}

// The 'max' is applied to each dimension, so this function is only useful for the color calculation
// and doesn't really have any intuative meaning. The resulting vector will fit inside of a 2,2,2 cube.
void VectorScalarMultiplyMax1(double vector[3], double scalar, double out_vector[3]) {
  out_vector[0] = std::min(vector[0] * scalar, 1.0);
  out_vector[1] = std::min(vector[1] * scalar, 1.0);
  out_vector[2] = std::min(vector[2] * scalar, 1.0);
}

double CalculatePhongShading(LightingParameters &lp, Vector view_dir, Vector normal) {
  /* 
   * This is a list of the variables we have to work with.
   normal;	    // The normal of this fragment
   view_dir.dir;	    // The direction of the viewer
   lp.lightDir;	    // The direction of the light source
   lp.Ks;            // The coefficient for specular lighting.
   lp.alpha;         // The exponent term for specular lighting.
   */
  // Lets transform all of our vector represnetations to use the Vector class, and normalize the normal and the view direction
  // The light direction does _not_ get normalized
  Vector N = normal;
  Vector L(lp.lightDir[0], lp.lightDir[1], lp.lightDir[2]);
  Vector V = view_dir;
  N.normalize();
  V.normalize();
  L.normalize();

  // Calculate Ambient
  double ambient = lp.Ka;

  // Calculate Diffuse
  double diffuse = lp.Kd * std::abs(Vector::Dot(N, L));
  //double diffuse = lp.Kd * std::abs(Vector::Dot(N, L));
  if (diffuse != diffuse || diffuse <= 0 || diffuse > 1.0) {
    LOG(WARNING) << "Have a Weird diffuse value! " << diffuse;// << N << L;
  }

  // Calculate Specular 
  Vector R =  (N * (Vector::Dot(L, N) * 2)) - L;
  double dot_v_r = Vector::Dot(V, R);
  double specular = 0.0;
  if (dot_v_r != 0.0) {
    specular = lp.Ks * std::pow(dot_v_r, lp.alpha);
  } 
  if (specular != specular) {
    LOG(WARNING) << "Specular value was NAN, setting to 0.0! " << specular;
    specular = 0.0;
  }
  specular = std::max(specular, 0.0);

  return ambient + diffuse + specular;
}

Triangle transformTriangle(Triangle orig_triangle, Matrix &transform, Vector &cam_position, LightingParameters &lp) {
    Point trans_points[3];
    double point[4]; 
    double trans_point[4]; 
    for (int i = 0; i < 3; i++) {
      // Setup variables
      point[0] = orig_triangle.X[i];
      point[1] = orig_triangle.Y[i];
      point[2] = orig_triangle.Z[i];
      point[3] = 1;
      // Calculate View Direction
      Vector vertex_position(orig_triangle.X[i], orig_triangle.Y[i], orig_triangle.Z[i]);
      Vector view_dir = vertex_position - cam_position;
      transform.TransformPoint(point, trans_point);
      trans_points[i] = Point(
	  Vector( trans_point[0] / trans_point[3], 
		  trans_point[1] / trans_point[3], 
		  trans_point[2] / trans_point[3]), 
	  orig_triangle.normals[i], 
	  CalculatePhongShading(lp, view_dir, orig_triangle.normals[i]),
	  orig_triangle.colors[i]);
    }
    return Triangle(trans_points[0], trans_points[1], trans_points[2], orig_triangle.id);
}

std::vector<Triangle> transformTriangles(std::vector<Triangle> triangles, Matrix &transform, Vector &cam_position, LightingParameters &lp) {
  std::vector<Triangle> ret;
  for (std::vector<Triangle>::iterator orig_triangle = triangles.begin(); orig_triangle != triangles.end() ; orig_triangle++) {
    // Construct a new triangle using the transform
    ret.push_back(transformTriangle(*orig_triangle, transform, cam_position, lp));
  }
  return ret;
}

void rasterizeTriangles(std::vector<Triangle> triangles, Screen &screen) {
  for (std::vector<Triangle>::iterator triangle = triangles.begin(); triangle != triangles.end() ; triangle++) {
    triangle->rasterize(screen);
  }
}

double ceil441(double f) { return ceil(f-0.00001); }

double floor441(double f) { return floor(f+0.00001); }

vtkImageData * NewImage(int width, int height) {
  vtkImageData *img = vtkImageData::New();
  img->SetDimensions(width, height, 1);
  img->AllocateScalars(VTK_UNSIGNED_CHAR, 3);

  return img;
}

void ClearImage(int width, int height, vtkImageData *img) {
  unsigned char *buffer = (unsigned char *) img->GetScalarPointer(0,0,0);
  int npixels = width*height;
  for (int i = 0 ; i < npixels*3 ; i++) {
    buffer[i] = 0;
  }
}

void WriteImage(vtkImageData *img, const char *filename) {
  std::string full_filename = filename;
  full_filename += ".png";
  vtkPNGWriter *writer = vtkPNGWriter::New();
  writer->SetInputData(img);
  writer->SetFileName(full_filename.c_str());
  writer->Write();
  writer->Delete();
}

std::vector<Triangle> GetTriangles100() {
  std::vector<Triangle> rv(100);

  unsigned char colors[6][3] = { {255,128,0}, {255, 0, 127}, {0,204,204},  {76,153,0}, {255, 204, 204}, {204, 204, 0}};

  for (int i = 0 ; i < 100 ; i++) {
    int idxI = i%10;
    int posI = idxI*100;
    int idxJ = i/10;
    int posJ = idxJ*100;
    int firstPt = (i%3);
    rv[i].X[firstPt] = posI;
    if (i == 50)
      rv[i].X[firstPt] = -10;
    rv[i].Y[firstPt] = posJ;
    rv[i].X[(firstPt+1)%3] = posI+99;
    rv[i].Y[(firstPt+1)%3] = posJ;
    rv[i].X[(firstPt+2)%3] = posI+i;
    rv[i].Y[(firstPt+2)%3] = posJ+10*(idxJ+1);
    if (i == 95)
      rv[i].Y[(firstPt+2)%3] = 1050;
    rv[i].colors[0][0] = (colors[i%6][0])/255.0;
    rv[i].colors[0][1] = (colors[i%6][1])/255.0;
    rv[i].colors[0][2] = (colors[i%6][2])/255.0;
    rv[i].colors[1][0] = (colors[i%6][0])/255.0;
    rv[i].colors[1][1] = (colors[i%6][1])/255.0;
    rv[i].colors[1][2] = (colors[i%6][2])/255.0;
    rv[i].colors[2][0] = (colors[i%6][0])/255.0;
    rv[i].colors[2][1] = (colors[i%6][1])/255.0;
    rv[i].colors[2][2] = (colors[i%6][2])/255.0;
  }

  return rv;
}

std::vector<Triangle> GetTriangles(std::string infile) {
  vtkPolyDataReader *rdr = vtkPolyDataReader::New();
  rdr->SetFileName(infile.c_str());
  cerr << "Reading" << endl;
  rdr->Update();
  cerr << "Done reading" << endl;
  if (rdr->GetOutput()->GetNumberOfCells() == 0)
  {
    cerr << "Unable to open file!!" << endl;
    exit(EXIT_FAILURE);
  }
  vtkPolyData *pd = rdr->GetOutput();
  /*
     vtkDataSetWriter *writer = vtkDataSetWriter::New();
     writer->SetInput(pd);
     writer->SetFileName("hrc.vtk");
     writer->Write();
     */

  int numTris = pd->GetNumberOfCells();
  vtkPoints *pts = pd->GetPoints();
  vtkCellArray *cells = pd->GetPolys();
  vtkDoubleArray *var = (vtkDoubleArray *) pd->GetPointData()->GetArray("hardyglobal");
  double *color_ptr = var->GetPointer(0);
  //vtkFloatArray *var = (vtkFloatArray *) pd->GetPointData()->GetArray("hardyglobal");
  //float *color_ptr = var->GetPointer(0);
  vtkFloatArray *n = (vtkFloatArray *) pd->GetPointData()->GetNormals();
  float *normals = n->GetPointer(0);
  std::vector<Triangle> tris(numTris);
  vtkIdType npts;
  vtkIdType *ptIds;
  int idx;
  for (idx = 0, cells->InitTraversal() ; cells->GetNextCell(npts, ptIds) ; idx++)
  {
    if (npts != 3)
    {
      cerr << "Non-triangles!! ???" << endl;
      exit(EXIT_FAILURE);
    }
    double *pt = NULL;
    pt = pts->GetPoint(ptIds[0]);
    tris[idx].X[0] = pt[0];
    tris[idx].Y[0] = pt[1];
    tris[idx].Z[0] = pt[2];
    tris[idx].normals[0][0] = normals[3*ptIds[0]+0];
    tris[idx].normals[0][1] = normals[3*ptIds[0]+1];
    tris[idx].normals[0][2] = normals[3*ptIds[0]+2];
    pt = pts->GetPoint(ptIds[1]);
    tris[idx].X[1] = pt[0];
    tris[idx].Y[1] = pt[1];
    tris[idx].Z[1] = pt[2];
    tris[idx].normals[1][0] = normals[3*ptIds[1]+0];
    tris[idx].normals[1][1] = normals[3*ptIds[1]+1];
    tris[idx].normals[1][2] = normals[3*ptIds[1]+2];
    pt = pts->GetPoint(ptIds[2]);
    tris[idx].X[2] = pt[0];
    tris[idx].Y[2] = pt[1];
    tris[idx].Z[2] = pt[2];
    tris[idx].normals[2][0] = normals[3*ptIds[2]+0];
    tris[idx].normals[2][1] = normals[3*ptIds[2]+1];
    tris[idx].normals[2][2] = normals[3*ptIds[2]+2];

    // 1->2 interpolate between light blue, dark blue
    // 2->2.5 interpolate between dark blue, cyan
    // 2.5->3 interpolate between cyan, green
    // 3->3.5 interpolate between green, yellow
    // 3.5->4 interpolate between yellow, orange
    // 4->5 interpolate between orange, brick
    // 5->6 interpolate between brick, salmon
    double mins[7] = { 1, 2, 2.5, 3, 3.5, 4, 5 };
    double maxs[7] = { 2, 2.5, 3, 3.5, 4, 5, 6 };
    unsigned char RGB[8][3] = { 
      { 71, 71, 219 },
      { 0, 0, 91 },
      { 0, 255, 255 },
      { 0, 128, 0 },
      { 255, 255, 0 },
      { 255, 96, 0 },
      { 107, 0, 0 },
      { 224, 76, 76 }
    };

    for (int j = 0 ; j < 3 ; j++)
    {
      float val = color_ptr[ptIds[j]];
      int r;
      for (r = 0 ; r < 7 ; r++)
      {
	if (mins[r] <= val && val < maxs[r])
	  break;
      }
      if (r == 7)
      {
	cerr << "Could not interpolate color for " << val << endl;
	exit(EXIT_FAILURE);
      }
      double proportion = (val-mins[r]) / (maxs[r]-mins[r]);
      tris[idx].colors[j][0] = (RGB[r][0]+proportion*(RGB[r+1][0]-RGB[r][0]))/255.0;
      tris[idx].colors[j][1] = (RGB[r][1]+proportion*(RGB[r+1][1]-RGB[r][1]))/255.0;
      tris[idx].colors[j][2] = (RGB[r][2]+proportion*(RGB[r+1][2]-RGB[r][2]))/255.0;
    }
  }

  return tris;
}
