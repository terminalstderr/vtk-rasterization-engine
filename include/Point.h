/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: "Point" Helper -- think of a point as a completely defined vertex in worldspace,
 * It maintains color, normal direction (normalized), and most importantly, position
 */
#ifndef CIS541_POINT_H
#define CIS541_POINT_H

#include <glog/logging.h>

#include "Vector.h"
#include "Utility.h"

class Point {
  public:
  Vector view_dir;
  Vector position;
  Vector normal;
  double color[3];
  double shading;
  Point(const Vector &position, const Vector &normal, const double shading, double red, double grn, double blue);
  Point(const Vector &position, const Vector &normal, const double shading, const double color[3]);
  Point(const Vector &position, const Vector &normal, const Vector &view_dir, const double red, const double grn, const double blue);
  Point(const Vector &position, const Vector &normal, const Vector &view_dir, const double color[3]);
  Point();
  Point operator-(const Point &rhs);
  static Point lerp(Point p1, Point p2, const double alpha);
  static Point lerpy(Point p1, Point p2, const double y_val);
  static Point lerpx(Point p1, Point p2, const double x_val);
};
#endif//CIS541_POINT_H
