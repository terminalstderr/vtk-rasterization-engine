/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Matrix Helper
 */
#ifndef CIS541_MATRIX_H
#define CIS541_MATRIX_H
#include <fstream>
class Matrix
{
  public:
    double A[4][4];

    void TransformPoint(const double *ptIn, double *ptOut);
    static Matrix ComposeMatrices(const Matrix &, const Matrix &);
    void Print(std::ostream &o);
    void set(unsigned int x, unsigned int y, double val);
    double get(unsigned int x, unsigned int y);
    void reset();
};
#endif//CIS541_MATRIX_H
