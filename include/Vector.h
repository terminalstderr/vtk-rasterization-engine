/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Vector Helper 
 */
#ifndef CIS541_VECTOR_H
#define CIS541_VECTOR_H

class Vector {
  public:
    double x, y, z;
    Vector();
    Vector(double x, double y, double z);
    Vector(double xyz[]);
    Vector(const Vector &other);
    //Vector(Vector other);

    Vector operator-(const Vector &rhs);
    Vector operator*(const double &rhs);
    Vector normalized();
    void normalize();
    static double Dot(const Vector &v1, const Vector &v2);
    static Vector Cross(const Vector &v1, const Vector &v2);
};
#endif//CIS541_VECTOR_H
