/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Screen Management
 */
#ifndef CIS541_SCREEN_H
#define CIS541_SCREEN_H
#include "Utility.h"

class Screen {
  private:
    bool 
      debug;
    unsigned int
      *triangle_ids;
    unsigned char   
      // buffer as _bytes_ e.g. buffer[0,1,2] corrospond to 1 pixel
      *buffer;
    double 
      *z_buffer;
    int 
      // width in pixels
      width, 
      // height in pixels 
      height;
    int coord_to_index(unsigned int x, unsigned int y);
    int zbuffer_coord_to_index(unsigned int x, unsigned int y);
  public:
    Screen(unsigned int _width, unsigned int _height, unsigned char *_buffer);
    Screen(unsigned int _width, unsigned int _height, unsigned char *_buffer, bool debug);
    ~Screen();

    /**
     * Get the zbuffer value of a pixel at a given location in this screen
     */
    double getZ(unsigned int x, unsigned int y);

    /**
     * Set the zbuffer value of a pixel at a given location in this screen
     */
    void setZ(unsigned int x, unsigned int y, double z);

    void setTriangleId(unsigned int x, unsigned int y, unsigned int id);
    void logScreenTriangleIds();

    /**
     * Get the color of a pixel at a given location in this screen
     */
    void getPixel(unsigned int x, unsigned int y, unsigned char (&ret_color)[3]);

    /**
     * Get the color of a pixel at a given location in this screen
     */
    void getPixel(unsigned int x, unsigned int y, double (&ret_color)[3]);

    /**
     * Set the color of a pixel at a given location in this screen
     */
    void setPixel(unsigned int x, unsigned int y, const double color[3]);

    /**
     * Set the color of a pixel at a given location in this screen
     */
    void setPixel(unsigned int x, unsigned int y, const unsigned char color[3]);

    /**
     * Clears all colors from the screen, setting back to black
     */
    void clear();

};

#endif//CIS541_SCREEN_H
