/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: 2D Triangle Primitive
 */
#ifndef CIS541_TRIANGLE_H
#define CIS541_TRIANGLE_H
#include "Utility.h"
#include "Point.h"
#include "Screen.h"

#define POINTS_PER_TRIANGLE 3

class Point;
class Screen;
class LightingParameters;

/**
 * A simple 2D triangle primitive in which all vertices have the same color, and 
 * vertices have only an X and Y component, without a Z component.
 */
class Triangle {
  public:
    friend std::ostream &operator<<(std::ostream &out, const Triangle &t);

    /**
     * Construct a Triangle with vertices based on the points p1, p2, and p3
     */
    Triangle(const Point &p1, const Point &p2, const Point &p3, unsigned int id);

    /**
     * Construct a Triangle without any values initialized values -- can be set directly as the X, Y, and color fields are mutable and public.
     */
    Triangle();

    unsigned int id;

    double        shading[POINTS_PER_TRIANGLE];
    /** 
     * X coordinates for the 3 vertices of this triangle 
     */
    double         X[POINTS_PER_TRIANGLE];
    /** 
     * Y coordinates for the 3 vertices of this triangle 
     */
    double         Y[POINTS_PER_TRIANGLE];
    /** 
     * Z coordinates for the 3 vertices of this triangle 
     */
    double         Z[POINTS_PER_TRIANGLE];
    /** 
     * A single RGB color for each of the 3 vertex of this triangle, think of it as the triangles vertex colors.
     */
    double colors[POINTS_PER_TRIANGLE][3]; // 3 channels for color
    /** 
     * A 3D vector normal value for each of the 3 vertex of this triangle, think of it as the triangles vertex normals.
     * The normal is a vector using Cartesian coordinate system, 0 => x, 1 => y, 2 => z
     */
    double normals[POINTS_PER_TRIANGLE][3]; // 3 channels for x, y, z

    /**
     * Rasterize (draw) this triangle to the screen provided.
     */
    bool rasterize(Screen &screen);

  //private:

    /**
     * Get the 'row' minimum for this triangle -- e.g. the value corresponding to the bottom 'row' that this triangle contains (where 'rows' are indexed using _whole_ numbers).
     *
     * Example: The triangle below will have a 'row minimum' of 5.0 corresponding to the ceiling of 4.25.
     *		      .
     *		      |\
     *	 	      | \
     *	 	      |  \
     * (32.0, 4.25)-> .---. <- (35.0, 4.25)
     *
     */
    double rowMin();

    /**
     * Get the 'row' maximum for this triangle -- e.g. the value corresponding to the top 'row' that this triangle contains (where 'rows' are indexed using _whole_ numbers).
     *
     * Example: The triangle below will have a 'row maximum' of 9.0 corresponding to the floor of 9.998.
     *
     *		      . <- (32.0, 9.998)
     *		      |\
     *	 	      | \
     *	 	      |  \
     *		      .---.
     *
     */
    double rowMax();

    /**
     * When we have selected 2 of three indices, getting the remaining index is a simple combination problems.
     * The two provided indices MUST be distinct values of the set S={0,1,2}, and returned will be the remaining
     * value of the set S that was not provided.
     *
     * Example: These are a set of nearly exhaustive examples excluding repetitive indices
     * getRemainingIndex(1,2);// -> 0
     * getRemainingIndex(0,2);// -> 1
     * getRemainingIndex(0,1);// -> 2
     */
    unsigned int getRemainingIndex(const unsigned int i1, const unsigned int i2);

    /**
     * Find the indices of this triangle that correspond to the bottommost (minimum y value), topmost (maximum y value), and
     * whatever index remains (middle y value).
     * This method is agnostic to whether this triangle is flat bottomed, flat topped, or arbitrary.
     *
     * Example: The following triangle would return bottom_index=2, top_index=0, middle_index=1
     *			      (top)
     *			    X[0], Y[0]
     *		      . <- (32.0, 9.998)
     *		      |\
     *	(middle)      | \
     *	X[1], Y[1]    |  \	  (bottom)
     * (32.0, 4.25)-> .   \      X[2], Y[2]
     *		       \---. <- (35.0, 3.25)
     */
    void getBottomTopMid(unsigned int &bottom_index, unsigned int &top_index, unsigned int &middle_index);

    /**
     * NOTICE -- This Triangle MUST be a flat bottomed for this method to succeed.
     * This method will extract the index of the left bottom (left index), right bottom (right index), and top (top index).
     *
     * Example: The following triangle would return left_index=0, right_index=1, top_index=2
     * Notice that in this example triangle, the 'top' point is actually the farthest left.
     *
     *			  (top)
     * 	          .  <- X[2], Y[2]
     * 	           \
     * 	           |\
     * 	            \\
     * 	            | \
     * 	            |  \
     * 	  (left)    |   \    (right)
     * X[0], Y[0]-> .----. X[1], Y[1]
     *
     */
    bool getLeftRightTop_flatBottom(unsigned int &left_index, unsigned int &right_index, unsigned int &top_index);
    /**
     * NOTICE -- This Triangle MUST be a flat topped for this method to succeed.
     * See getLeftRightTop_flatBottom
     */
    bool getLeftRightBottom_flatTop(unsigned int &left_index, unsigned int &right_index, unsigned int &bottom_index);

    /**
     * NOTICE -- This Triangle MUST be a flat bottomed for this method to succeed.
     * Rasterize (draw) this flat bottomed triangle to the screen provided
     */
    bool rasterize_flatBottom(Screen &screen);
    /**
     * NOTICE -- This Triangle MUST be a flat topped for this method to succeed.
     * See rasterize_flatBottom
     */
    bool rasterize_flatTop(Screen &screen);

    void scanline(Screen &screen, unsigned int li, unsigned int ri, unsigned int ti);

};
#endif//CIS541_TRIANGLE_H
