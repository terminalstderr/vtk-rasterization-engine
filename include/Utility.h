/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Utility functions
 */
#ifndef CIS541_UTILITY_H
#define CIS541_UTILITY_H
#include <vtkImageData.h>
#include <vtkPNGWriter.h>
#include <vector>
#include "Screen.h"
#include "Triangle.h"
#include "Vector.h"
#include "Point.h"
#include "Matrix.h"

#define BYTES_PER_PIXEL 3
#define RED_CH 0
#define GRN_CH 1
#define BLU_CH 2

class Screen;
class Triangle;

struct LightingParameters {
  LightingParameters() {
    lightDir[0] = -0.6;
    lightDir[1] = 0;
    lightDir[2] = -0.8;
    Ka = 0.3;
    Kd = 0.7;
    Ks = 5.3;
    alpha = 7.5;
  };

  LightingParameters(double xdir, double ydir, double zdir, double Ka, double Kd, double Ks, double alpha) {
    this->lightDir[0] = xdir;
    this->lightDir[1] = ydir;
    this->lightDir[2] = zdir;
    this->Ka = Ka;
    this->Kd = Kd;
    this->Ks = Ks;
    this->alpha = alpha;
  };

  double lightDir[3]; // The direction of the light source
  double Ka;           // The coefficient for ambient lighting.
  double Kd;           // The coefficient for diffuse lighting.
  double Ks;           // The coefficient for specular lighting.
  double alpha;        // The exponent term for specular lighting.
};

Triangle transformTriangle(Triangle orig_triangle, Matrix &transform, Vector &cam_position, LightingParameters &lp);

std::vector<Triangle> transformTriangles(std::vector<Triangle> triangles, Matrix &transform, Vector &cam_position, LightingParameters &lp);

double angle_to_radian(double angle);

double cot(double angle_rad);

void VectorScalarMultiplyMax1(double vector[3], double scalar, double out_vector[3]);

double CalculatePhongShading(LightingParameters &lp, Vector view_dir, Vector normal);

/**
 * Take all of the triangles provided in the triangles vector and draw them to the Screen provided.
 */
void rasterizeTriangles(std::vector<Triangle> triangles, Screen &screen);

/**
 * Calculates the ceiling of a double function in a non-platform-specific manner
 */
double ceil441(double f);

/**
 * Calculates the floor of a double function in a non-platform-specific manner
 */
double floor441(double f);

/** 
 * A VTK wrapper function that allocates an image 
 * (which can be used as a drawing surface, e.g. a 'canvas' or 'screen')
 */
vtkImageData * NewImage(int width, int height);

/**
 * Sets all the pixels in the provided image to have color {0,0,0}
 */
void ClearImage(int width, int height, vtkImageData *img);

/**
 * Writes a produced image to disk as a png.
 */
void WriteImage(vtkImageData *img, const char *filename);

/**
 * Gets (builds) Triangles using black-mathgic
 */
std::vector<Triangle> GetTriangles(std::string filename);

/**
 * Gets (builds) Triangles using black-mathgic
 */
std::vector<Triangle> GetTriangles_goDucks(void);

/**
 * Gets (builds) Triangles using black-mathgic
 */
std::vector<Triangle> GetTriangles100(void);

#endif//CIS541_UTILITY_H
