/**
 * Ryan Leonard
 * Fall 2016
 * 
 * Computer Graphics: Camera class is a container for all camera's details
 */
#ifndef CIS541_CAMERA_H
#define CIS541_CAMERA_H
#include "Matrix.h"
#include "Vector.h"
#include "Utility.h"

class Camera {
  public:
    double near;
    double far;
    double angle;
    double position[3];
    double focus[3];
    double up[3];

    static Camera GetCamera(int frame, int nframes);
    static double SineParameterize(int curFrame, int nFrames, int ramp);
    Vector getViewDirection();
    Matrix ViewTransform();
    Matrix CameraTransform();
    Matrix DeviceTransform(unsigned int width, unsigned int height);
};
#endif//CIS541_CAMERA_H
