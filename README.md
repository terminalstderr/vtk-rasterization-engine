# VTK Rasterization Engine #

This project is an implementation of a basic software Rasterization Engine implemented in C++. The underlying library VTK gives us a canvas to draw on. Google testing and logging libraries were used during development.



## Dependencies ##
Depends on VTK visualization library, glog logging library, and gtest testing library.

http://www.vtk.org/download/

https://github.com/google/glog/blob/master/INSTALL

https://github.com/google/googletest

## Building ##

This project was primarily developed on a Windows 10 machine within the Bash-on-Ubuntu-on-Windows shell environment. This project was partially developed and tested on a Linux server running Ubuntu 16.04.

A basic build process would be:

```
#!bash

$ git clone https://terminalstderr@bitbucket.org/terminalstderr/vtk-rasterization-engine.git
$ cd vtk-rasterization-engine
$ export VTK_DIR=<VTK_path>/VTK-6.3.0
$ cmake CMakeLists.txt
$ make

```

 

## Basic Usage ##

```
#!bash
./project1 [-i input.vtk] [-o output_fname] [-w 1000] [-h 1000] [-b 12345] [-s 100] [-d] [-z]

OPTIONS
  -i Input filename
      The provided input file -- normally a .vtk file.

  -o Output filename
      The output filename _without_ the extension suffix.

  -w Width
      The height of the screen we will be outputting to.

  -h Height
      The height of the screen we will be outputting to.

  -d Debug
      Extra breadcrumbs will be printed to Glog info file (normally found at /tmp/project1.INFO).

  -b BeginNumber
      A debugging tool -- will print triangles starting at the begining number. If you find that
      there is a bogus triangle based on investigation of breadcrumbs provided by -d flag, you can
      attempt to further debug by rasterizing _only_ that one triangle (or that triangle and its
      fellow 'nearby' triangles by using the -s flag as well).

  -s Size
      Pairs with the -b flag to print a range of triangles instead of one particular triangle. This
      _only_ works in conjunction with the -b flag, and cannot be used alone. For behavior as you 
      might expect to use this flag alone, use the option "-b 0" in conjunction with some '-s 2000'.

  -z Old (deprecated)
      Uses internally generated geometry instead of getting geometry from an input file.

```